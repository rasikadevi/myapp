import React from 'react';
import {Router, Scene, Stack} from 'react-native-router-flux';
import SplashScreen from 'react-native-splash-screen';
import {View, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import redux from './redux';

/**
 * The root component
 * Contains the screen configuration for react-native-router-flux
 */
export default class Route extends React.PureComponent {
  /**
   * @return {void}
   */
  componentDidMount = () => {
    SplashScreen.hide();
  };

  /**
   * @return {JSX.Element}
   */
  render = () => (
    <Provider store={redux.store}>
      <PaperProvider>
        <Router>
          <Scene key="root">
            <Scene
              key="login"
              component={require('./screens/LoginScreen').default}
              initial
              hideNavBar
            />

            <Scene
              key="search"
              component={require('./screens/SearchScreen').default}
              hideNavBar
            />
          </Scene>
        </Router>
      </PaperProvider>
    </Provider>
  );
}
