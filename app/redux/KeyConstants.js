const KeyConstants = {
  LOGIN_USER: 'LOGIN_USER',
  SAVE_LIST:'SAVE_LIST',
  LOAD_LIST:'LOAD_LIST'
};

export default KeyConstants;
