import axios from 'axios';
import {REHYDRATE} from 'redux-persist';
import {Actions} from 'react-native-router-flux';
import KeyConstants from './KeyConstants';
import Config from 'react-native-config';
const {LOGIN_USER, SAVE_LIST, LOAD_LIST} = KeyConstants;

// Actions
const userIDKey = 'USER_ID';
const oppoBannerKey = 'oppoBanner';

// Initial State
export const UserInitialState = {
  userID: '',
  locationList: [{city: 'Testing data', temp: 'dcfe', favourite: false}],
};
const saveListToRedux = list => ({
  type: SAVE_LIST,
  payload: list,
});
const loadListToRedux = () => ({
  type: LOAD_LIST,
});
export const loginUser = (email, password) => {
  return async dispatch => {
    try {
      const data = {
        email,
        password,
      };
      Actions.push('search');
      return true;
    } catch (error) {
      return error;
    }
  };
};
export const listFavourite = index => {
  return async dispatch => {
    try {
      const list = UserInitialState.locationList;
      list[index].favourite = !list[index].favourite;
      return dispatch(saveListToRedux(list));
    } catch (error) {
      return error;
    }
  };
};
export const loadList = () => {
  return async dispatch => {
    try {
      dispatch(loadListToRedux());

      return dispatch(saveListToRedux(UserInitialState.locationList));
    } catch (error) {
      return error;
    }
  };
};

export const addList = (address, currentLongitude, currentLatitude) => {
  return async dispatch => {
    try {
      const list = UserInitialState.locationList;

      axios
        .get(
          `${Config.API_URL}/weather?lat=${currentLatitude}&lon=${currentLongitude}&exclude=hourly,daily&appid=${Config.API_APP_KEY}`,
        )
        .then(function (response) {
          const select = {
            city: address,
            temp: 'temprature :' + JSON.stringify(response.data.main.temp) ,
            favourite: false,
          };
          list.push(select);
          return dispatch(saveListToRedux(list));
        })
        .catch(function (error) {
          console.error(error);
        });

   
    } catch (error) {
      return error;
    }
  };
};

// Reducer
export const UserReducer = (state = UserInitialState, action) => {
  // Destructure state object
  let {userID, locationList} = state;

  switch (action.type) {
    case REHYDRATE:
      if (action.payload) {
        userID = action.payload.user.userID;
        locationList = action.payload.user.locationList;
      }
      break;

    case LOGIN_USER:
      userID = action.payload;
      break;
    case SAVE_LIST:
      locationList = action.payload;
      break;
  }

  return {
    userID,
    locationList,
  };
};
