import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, StyleSheet, Image} from 'react-native';
import {IconButton, TextInput, Button, Text} from 'react-native-paper';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import {loginUser} from './../redux/UserReducer';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropTypes from "prop-types";
// eslint-disable-next-line valid-jsdoc
/**
 * The welcome banner component
 */
function LoginScreen({actions}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {}, []);

  const loginMethod = () => {
   actions.loginUser(email,password);
  };

  /**
   * @return {JSX.Element}
   */
  return (
    <View style={styles.container}>
      <Image source={require('./../../assets/img/logo.png')} />
      <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          placeholder="Email"
          placeholderTextColor="#003f5c"
          onChangeText={text => setEmail(text)}
          underlineColor="transparent"
          selectionColor="black"
          theme={{colors: {primary: 'transparent', text: 'black'}}}
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          placeholder="Password"
          placeholderTextColor="#003f5c"
          onChangeText={text => setPassword(text)}
          underlineColor="transparent"
          selectionColor="black"
          secureTextEntry={true}
          theme={{colors: {primary: 'transparent', text: 'black'}}}
        />
      </View>
      <TouchableOpacity>
        <Text style={styles.forgot}>Forgot Password?</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.loginBtn}
        onPress={() => {
          loginMethod();
        }}>
        <Text style={styles.loginText}>LOGIN</Text>
      </TouchableOpacity>
    </View>
  );
}

// LoginScreen.propTypes = {};

const mapStateToProps = state => ({
  user: state.user,
  actions: PropTypes.object,
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginUser,
    },
    dispatch,
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#05a7e5',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    fontWeight: 'bold',
    fontSize: 50,
    color: '#fb5b5a',
    marginBottom: 40,
  },
  inputView: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 20,
  },
  inputText: {
    height: 46,
    color: 'red',
    backgroundColor: 'white',
  },
  forgot: {
    color: 'white',
    fontSize: 11,
  },
  loginBtn: {
    width: '80%',
    backgroundColor: '#213a74',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
    marginBottom: 10,
  },
});
