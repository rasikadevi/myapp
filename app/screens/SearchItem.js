import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, StyleSheet, Image} from 'react-native';
import {IconButton, TextInput, Button, Text} from 'react-native-paper';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import {loginUser, listFavourite, loadList} from './../redux/UserReducer';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import PropTypes from 'prop-types';
// eslint-disable-next-line valid-jsdoc
/**
 * The welcome banner component
 */
function SearchItem({user, actions, item,index,listFavourite}) {
  const [fav, setfav] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {}, []);

  const loginMethod = () => {
    actions.loginUser(email, password);
  };

  /**
   * @return {JSX.Element}
   */
  return (
    <View style={styles.container}>
      <View style={{flex: 1, flexDirection: 'row'}}>
        <View style={{flex: 1, flexDirection: 'column'}}>
          <Text style={styles.cityStyle}>{item.city}</Text>
          <Text style={styles.tempStyle}>{item.temp}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {listFavourite()
          }}>
          <Icon
            name={item.favourite ? 'heart' : 'heart-outline'}
            color="red"
            size={22}
            style={{alignSelf: 'center'}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

SearchItem.propTypes = {
  user: PropTypes.object,
  actions: PropTypes.object,
  item: PropTypes.object,
  index:PropTypes.number,
  listFavourite:PropTypes.func
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginUser,
      listFavourite,
      loadList,
    },
    dispatch,
  ),
});

export default connect(null, mapDispatchToProps)(SearchItem);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#05a7e5',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  logo: {
    fontWeight: 'bold',
    fontSize: 50,
    color: '#fb5b5a',
    marginBottom: 40,
  },
  inputView: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 20,
  },
  inputText: {
    height: 46,
    color: 'red',
    backgroundColor: 'white',
  },
  forgot: {
    color: 'white',
    fontSize: 11,
  },
  loginBtn: {
    width: '80%',
    backgroundColor: '#213a74',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
    marginBottom: 10,
  },
  cityStyle: {
    fontSize: 13,
    fontWeight: 'bold',
  },
  tempStyle: {
    fontSize: 11,
  },
});
