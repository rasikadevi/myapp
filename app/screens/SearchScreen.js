import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
  ScrollView,
} from 'react-native';
import {
  IconButton,
  TextInput,
  Button,
  Text,
  Searchbar,
  Divider,
  Appbar,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import AwesomeAlert from 'react-native-awesome-alerts';
import SearchItem from './SearchItem';
import RNLocation from 'react-native-location';
import Geocoder from 'react-native-geocoder';
//import all the components we are going to use.
import Geolocation from '@react-native-community/geolocation';
import {
  loginUser,
  loadList,
  addList,
  listFavourite,
} from './../redux/UserReducer';
// eslint-disable-next-line valid-jsdoc
/**
 * The welcome banner component
 */
function SearchScreen({user, actions}) {
  const [showAlert, setShowAlert] = useState(false);
  const [showAlertLoading, setShowAlertLoading] = useState(false);
  const [noRecord, setNoRecord] = useState(false);
  const [city, setCity] = useState('');
  const [temp, setTemp] = useState('');
  const [list, setList] = useState();
  const [viewLocation, isViewLocation] = useState([]);
  const [search, setSearch] = useState([]);
  useEffect(() => {
    permissionHandle();
    actions.loadList();
    setList(user.locationList);
  }, []);

  useEffect(() => {
    if (search.length > 0) {
      const tmplist = list.filter(item => {
        return (
          item.city.toLowerCase().includes(search.toLowerCase()) ||
          item.city.toLowerCase().includes(search.toLowerCase())
        );
      });
      setNoRecord(tmplist.length > 0 && search.length < 3 ? false : true);
      setList(tmplist);
    } else {
      setList(user.locationList);
    }
  }, [search]);
  const permissionHandle = async () => {
    if (Platform.OS === 'ios') {
      // your code using Geolocation and asking for authorisation with
      Geolocation.requestAuthorization();
      const locationConfig = {authorizationLevel: 'whenInUse'};
      Geolocation.setRNConfiguration(locationConfig);
    }
    let permission = await RNLocation.checkPermission({
      ios: 'whenInUse', // or 'always'
      android: {
        detail: 'coarse', // or 'fine'
      },
    });
    if (!permission) {
      permission = await RNLocation.requestPermission({
        ios: 'whenInUse',
        android: {
          detail: 'coarse',
          rationale: {
            title: 'We need to access your location',
            message: 'We use your location to show where you are on the map',
            buttonPositive: 'OK',
            buttonNegative: 'Cancel',
          },
        },
      });
      getCurrentLocation();
    } else {
      getCurrentLocation();
    }
  };
  const getCurrentLocation = () => {
    Geolocation.getCurrentPosition(
      position => {
        //getting the Longitude from the location json
        const currentLongitude = JSON.stringify(position.coords.longitude);
        //getting the Latitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);
        var pos = {
          lat: parseFloat(currentLatitude),
          lng: parseFloat(currentLongitude),
        };
        Geocoder.geocodePosition(pos)
          .then(res => {
            setCity(res[0].formattedAddress);
            updateLocation(
              res[0].formattedAddress,
              currentLongitude,
              currentLatitude,
            );
          })
          .catch(error => console.error(error));
      },
      error => {
      },
      {
        enableHighAccuracy: false,
        maximumAge: 1000,
      },
    );
  };

  const updateLocation = (city, currentLongitude, currentLatitude) => {
    if (currentLongitude != null && currentLatitude != null) {
      actions.addList(city, currentLongitude, currentLatitude);
      actions.loadList();
      setList(user.locationList);
      setCity(list[list.length - 1].city);
      setTemp(list[list.length - 1].temp);
      setShowAlert(true);
    }
  };
  /**
   * @return {JSX.Element}
   */
  return (
    <View style={styles.container}>
      <Divider />
      <View
        style={{
          flexDirection: 'row',
          borderColor: 'white',
          backgroundColor: 'white',
          borderRadius: 20,
          borderWidth: 1,
          marginHorizontal: 18,
          paddingHorizontal: 10,
          marginVertical: 40,
          height: 50,
          overflow: 'hidden',
        }}>
        <Icon
          name="magnify"
          color="gray"
          size={22}
          style={{alignSelf: 'center'}}
        />
        <TextInput
          style={{
            color: 'black',
            borderWidth: 0,
            height: 55,
            overflow: 'hidden',
            backgroundColor: '#fff',
          }}
          onChangeText={text => setSearch(text)}
          value={search}
          placeholder="Search"
          selectionColor="black"
          underlineColorAndroid="transparent"
          theme={{colors: {text: 'black', primary: 'transparent'}}}
        />
      </View>
      <View
        style={{
          marginRight: 20,
        }}>
        <TouchableOpacity onPress={() => getCurrentLocation()}>
          <Button
            color="black"
            style={{
              backgroundColor: 'white',
              width: 200,
              alignSelf: 'flex-end',
              color: 'black',
            }}
            accessibilityLabel="Learn more about this purple button">
            <Text>Update Location</Text>
          </Button>
        </TouchableOpacity>
      </View>
      <ScrollView style={{flexGrow: 1}} scrollEnabled={true}>
        <FlatList
          data={list}
          renderItem={({item, index}) => (
            <SearchItem
              item={item}
              index={index}
              listFavourite={() => {
                actions.listFavourite(index);
                actions.loadList();
              }}
            />
          )}
          ItemSeparatorComponent={() => <Divider />}
          contentContainerStyle={{flexGrow: 1}}
          scrollEnabled={false}
        />
      </ScrollView>
      <AwesomeAlert
        show={noRecord}
        showProgress={false}
        title=""
        message="No Record Found"
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
        confirmText="Close"
        showConfirmButton={true}
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          setNoRecord(false);
        }}
      />
      <AwesomeAlert
        show={showAlert}
        showProgress={false}
        title={city}
        message={temp}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
        confirmText="Add to favourit"
        showConfirmButton={true}
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          listFavourite(list.length - 1);
          setShowAlert(false);
        }}
      />
    </View>
  );
}

SearchScreen.propTypes = {
  user: PropTypes.object,
  actions: PropTypes.object,
};
const mapStateToProps = state => ({
  user: state.user,
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginUser,
      loadList,
      addList,
      listFavourite,
    },
    dispatch,
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#05a7e5',
  },
  logo: {
    fontWeight: 'bold',
    fontSize: 50,
    color: '#fb5b5a',
    marginBottom: 40,
  },
  inputView: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 20,
  },
  inputText: {
    height: 46,
    color: 'red',
    backgroundColor: 'white',
  },
  forgot: {
    color: 'white',
    fontSize: 11,
  },
  loginBtn: {
    width: '80%',
    backgroundColor: '#213a74',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
    marginBottom: 10,
  },
});
