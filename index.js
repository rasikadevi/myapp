/** @format */

import {AppRegistry, StatusBar,LogBox} from 'react-native';
import App from './app/Route';
import {name as appName} from './app.json';

StatusBar.setBarStyle('dark-content');
AppRegistry.registerComponent(appName, () => App);
LogBox.ignoreAllLogs();